#include <cmath>
#include <iostream>
#include <string>

#include "batch.h"
#include "QuadMeshConfig.h"

#ifdef USE_MYMATH
#  include "MathFunctions.h"
#endif

#include "Eigen/Core"

typedef Eigen::Matrix<int32_t, 2, 1> Vector2i;

int main(int argc, char** argv) {
  std::cout << "Hello, world!\n";

  // report version
  std::cout << argv[0] << " Version " << QuadMesh_VERSION_MAJOR << "."
            << QuadMesh_VERSION_MINOR << std::endl;
  std::cout << "Usage: " << argv[0] << " number" << std::endl;

  batch_process(1, 2);

  Vector2i v(1, 2);
  std::cout<< "Vector2i: "<< v.x() << "," << v.y() <<std::endl;

  // convert input to double
  const double inputValue = std::stod(argv[1]);

  // calculate square root
  // const double outputValue = sqrt(inputValue);
  #ifdef USE_MYMATH
    const double outputValue = mysqrt(inputValue);
  #else
    const double outputValue = sqrt(inputValue);
  #endif
  std::cout << "The square root of " << inputValue << " is " << outputValue
            << std::endl;
}
