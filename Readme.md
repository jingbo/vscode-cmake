This is a working skeleton code to use VS Code as the editor, and use CMake to build.

## Setup

### VS Code

+ install extensions: CMake, CMake Tools (v1.2.2).

### CMake

+ CMake version 3.16.0-rc3
+ Learn from [CMake Tutorial](https://cmake.org/cmake/help/latest/guide/tutorial/index.html), and [source code](https://github.com/Kitware/CMake/tree/master/Help/guide/tutorial)


### build

```
cd <this_repo>
mkdir build
```
 
## TODOs
- [x] Build main
- [x] Debug and breakpoint
- [x] Build multiple file
- [x] Use dependancies
- [ ] Use external library

